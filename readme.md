# iLuxa  

Репозиторий микропроекта, который устроил бугурт одному человеку и дал трем другим улыбку на пару дней.  
С учетом того, что на сайт не было потрачено рубля, то проект полностью окупил себя.

### Как запустить локально

`git clone https://gitlab.com/Akiyamov/iluxa.git`  
Создайте виртуальную среду для python  
`pip install -r requirements.txt`  
В файле `locallibrary\setings.py` замените строчку  
`DEBUG` с 0 на 1  
`python manage.py runserver`

### Полный список библиотек для запуска
```
asgiref
django-crispy-forms
dj-database-url
Django
django-filter
django-tables2
gunicorn
psycopg2
sqlparse
tzdata
whitenoise
```
