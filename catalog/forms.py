from django import forms
from django.forms import ModelForm

from catalog.models import ilaksa

class CreateFrom(ModelForm):
    name = forms.CharField(max_length=200, label="Название")
    name.widget.attrs.update({'autocomplete': 'off'})
    descr = forms.CharField(max_length=1000, required=False, label="Краткое описание идеи")
    descr.widget.attrs.update({'autocomplete': 'off'})
    date_start = forms.DateField(input_formats=['%d/%m/%Y'])
    date_start.widget.attrs.update({'autocomplete': 'off'})

    STATUS_IDEA = (
        ('t', 'Реальна'),
        ('z', 'Забил хуй'),
    )

    status_idea = forms.ChoiceField(choices=STATUS_IDEA,
    label="Статус идеи")
    date_finish = forms.DateField(required=False, input_formats=['%d/%m/%Y'])
    date_finish.widget.attrs.update({'autocomplete': 'off'})
    class Meta:
        model = ilaksa
        fields = ['name', 'descr', 'date_start', 'status_idea', 'date_finish']

class EditForm(ModelForm):
    new_descr = forms.CharField(max_length=1000, required=False, label="Краткое описание идеи")
    STATUS_IDEA = (
        ('t', 'Реальна'),
        ('z', 'Забил хуй'),
    )

    status_idea = forms.ChoiceField(choices=STATUS_IDEA,
    label="Статус идеи")
    date_finish = forms.DateField(required=False, input_formats=['%d/%m/%Y'])
    date_finish.widget.attrs.update({'autocomplete': 'off'})
    class Meta:
        model = ilaksa
        fields = ['descr', 'status_idea', 'date_finish']