from dataclasses import fields
from urllib import request
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from catalog.models import ilaksa
from .forms import CreateFrom, EditForm
from .tables import ProductTable
from django_tables2 import SingleTableView
from django.views.generic.edit import UpdateView

def index(request):
    num_idea=ilaksa.objects.all().count()
    num_live_idea=ilaksa.objects.filter(status_idea='t').count()
    num_dead_idea=ilaksa.objects.filter(status_idea='z').count()

    return render(
        request,
        'index.html',
        context={'num_idea':num_idea,'num_live_idea':num_live_idea, 'num_dead_idea':num_dead_idea},
    )

class LiveIdeaTableView(SingleTableView):
    model = ilaksa
    table_class = ProductTable

def ilaksa_new(request):
    if request.method == 'POST':
        form = CreateFrom(request.POST)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect(reverse(index))
    else:
        form = CreateFrom()
    return render(
        request,
        'ilaksa_new.html',
        {'form': form}
    )

def EditData(request, pk):
    ilaksa_inst = get_object_or_404(ilaksa, pk=pk)
    if request.method == 'POST':
        form = EditForm(request.POST)
        if form.is_valid():
            ilaksa_inst.descr = form.cleaned_data["descr"]
            ilaksa_inst.status_idea = form.cleaned_data["status_idea"]
            ilaksa_inst.date_finish = form.cleaned_data["date_finish"]
            ilaksa_inst.save()
        return HttpResponseRedirect(reverse(index))
    else:
        form = EditForm()
    return render(
        request,
        'catalog/edit_data.html',
        {'form': form}
    )