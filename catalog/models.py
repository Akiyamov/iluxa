from django.db import models
import uuid

# Create your models here.

class ilaksa(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=200, help_text="Краткое описание идеи", verbose_name="Название")
    descr = models.TextField(max_length=1000, help_text="Описание идеи подробнее", blank=True, verbose_name="Описание")
    date_start = models.DateField(verbose_name="Дата начала")

    STATUS_IDEA = (
        ('t', 'Реальна'),
        ('z', 'Забил хуй'),
    )

    status_idea = models.CharField(max_length=1, choices=STATUS_IDEA, default='t',
    help_text='Что с идеей Айлаксы сейчас', verbose_name="Статус идеи")
    date_finish = models.DateField(null=True, blank=True, verbose_name="Дата окончания")

    class Meta:
        ordering = ["date_start"]

    def __str__(self):
        return f'{self.id} ({self.name})'
    